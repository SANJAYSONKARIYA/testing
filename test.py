# import unittest
# from main import sanjay

# class sum_test(unittest.TestCase):
#     def t_sum(self):
#         c = sanjay(5, 4)
#         d = c.sum()
#         self.assertEqual(d, 9, "sum is wrong")

# if __name__ == '__main__':
#     unittest.main()

# test.py

import unittest
from main import maths

class sum_test(unittest.TestCase):
    def test_sum(self):
        # Create an instance of the maths class
        c = maths(5, 4)
        
        # Call the sum method on the instance
        # c = math_instance.sum()
        
        # Use assertEqual to check if the sum is 9
        self.assertEqual(c.sum(), 9, "sum is wrong")

    def test_diff(self):
        d = maths(25, 5)
        self.assertEqual(d.diff(), 5, "Difference is wrong")

    def test_prod(self):
        p = maths(3, 6)
        self.assertEqual(p.prod(), 18,"Product is wrong")

    def test_sub(self):
        s = maths(20, 10)
        self.assertEqual(s.sub(), 10, "Subtraction is wrong")

if __name__ == '__main__':
    unittest.main()
